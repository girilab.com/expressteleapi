const dotenv = require('dotenv');
const axios = require('axios');

const KEY = process.env.GOOGLE_MAP_PLATFORM_API_KEY;
const COUNTRY = 'France';
const PLACE_TEXT_SEARCH_API = "https://maps.googleapis.com/maps/api/place/textsearch/json";
const PLACE_DETAILS_API = "https://maps.googleapis.com/maps/api/place/details/json";

/**
  * Gets the company general data using the Google Map Platform.
  */
exports.searchCompanyByName = async (companyName) => {
  const {data} = await axios.get(
    `${PLACE_TEXT_SEARCH_API}?query=${companyName}+${COUNTRY}&type=point_of_interest&key=${KEY}`
  );
  return data;
}

/**
  * Gets the company specific data based on company Id using the Google Map Platform.
  */
exports.searchDetailsByCompanyId = async (companyId) => {
  const details = await axios.get(
    `${PLACE_DETAILS_API}?placeid=${companyId}&key=${KEY}`
  );
  return details;
}
