const chai = require('chai');
const { stub } = require('sinon');
const sinonChai = require('sinon-chai');
const chaiHttp = require('chai-http');
const proxyquire = require('proxyquire');
const server = require('../server');

chai.use(sinonChai);
chai.use(chaiHttp);

describe('Test API Route GET /api/v1/telephone', () => {
  let mockCompanyService;
  let telephoneController;

  beforeEach(() => {
    mockCompanyService = {
      searchCompanyByName: stub(),
      searchDetailsByCompanyId: stub()
    }
    telephoneController = proxyquire('../controllers/telephone.controller', {
      '../services/places.service': mockCompanyService
    });
  });

  afterEach(() => {
    mockCompanyService.searchCompanyByName.resetHistory();
    mockCompanyService.searchDetailsByCompanyId.resetHistory();
  });

  it('it should GET the response with telephone number for route /api/v1/telephone', (done) => {
    const telephoneNumber = '04 50 34 63 54';
    const resultTextSearch = {
      results: [
        {
          name: 'Experdeco',
          place_id: 'abc123'
        }
      ]
    };
    const resultCompanyIdSearch = {
      data: {
        result: {
          place_id: 'abc123',
          formatted_phone_number: telephoneNumber
        }
      }
    }
    mockCompanyService.searchCompanyByName.returns(Promise.resolve(resultTextSearch));
    mockCompanyService.searchDetailsByCompanyId.returns(Promise.resolve(resultCompanyIdSearch));
    chai.request(server)
      .get('/api/v1/telephone?company=experdeco')
      .end((err, res) => {
        chai.expect(res.statusCode).to.equal(200);
        chai.expect(res.body.status).to.equal('success');
        chai.expect(res.body.telephone).to.equal(telephoneNumber);
        done();
      });
  });

  it('it should fail on missing url parameter "company"', (done) => {
    chai.request(server)
      .get('api/v1/telephone')
      .end((err, res) => {
        chai.expect(res.statusCode).to.equal(404);
        done();
      });
  });

  it('it should fail for unknown routes', (done) => {
    chai.request(server)
      .get('/api/v2')
      .end((err, res) => {
        chai.expect(res.statusCode).to.equal(404);
        chai.expect(res.body.status).to.equal('fail');
        chai.expect(res.body.message).to.equal('Cannot find /api/v2 on this server!');
        done();
      });
  });
});
