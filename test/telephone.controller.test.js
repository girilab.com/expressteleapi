const chai = require('chai');
const { stub, match } = require('sinon');
chai.use(require('sinon-chai'));
const { mockRequest, mockResponse } = require('mock-req-res');
const proxyquire = require('proxyquire');

describe('Test telephone controller', () => {
  const originalLogFunction = console.log;

  let mockCompanyService;
  let telephoneController;
  let req, res, next

  beforeEach(() => {
    companyName = 'Experdeco';
    req = mockRequest({ query: { company: companyName }});
    res = mockResponse();
    next = stub();

    mockCompanyService = {
      searchCompanyByName: stub(),
      searchDetailsByCompanyId: stub()
    }
    telephoneController = proxyquire('../controllers/telephone.controller', {
      '../services/places.service': mockCompanyService
    });

    // Optional: Supress console.log in test results
    console.log = () => {};
  });

  afterEach(() => {
    mockCompanyService.searchCompanyByName.resetHistory();
    mockCompanyService.searchDetailsByCompanyId.resetHistory();
    res.json.resetHistory();
  })

  it('called res.json with success', async () => {
    const resultTextSearch = {
      results: [
        {
          name: 'Experdeco',
          place_id: 'abc123'
        }
      ]
    };
    const resultCompanyIdSearch = {
      data: {
        result: {
          place_id: 'abc123',
          formatted_phone_number: '04 50 34 63 54'
        }
      }
    }
    mockCompanyService.searchCompanyByName.returns(Promise.resolve(resultTextSearch));
    mockCompanyService.searchDetailsByCompanyId.returns(Promise.resolve(resultCompanyIdSearch));

    const expectedJSON = { status: 'success', telephone: '04 50 34 63 54' };

    await telephoneController.getTelephoneNumber(req, res, next);

    chai.expect(next).to.not.have.been.called;
    chai.expect(res.json).to.have.been.calledWith(expectedJSON);
  });

  it('fail on no data from google map platform API', async () => {
    mockCompanyService.searchCompanyByName.returns(null);
    mockCompanyService.searchDetailsByCompanyId.returns(null);

    const expectedError = 'No company found using this API';

    await telephoneController.getTelephoneNumber(req, res, next);

    const nextFirstCallArgument = next.getCall(0).args[0];

    chai.expect(next).to.be.calledTwice;
    chai.expect(nextFirstCallArgument.message).to.equal(expectedError);
  });

  it('fail on unexpected error', async () => {
    mockCompanyService.searchCompanyByName.throws();
    const expectedError = 'Oops. Something went wrong!';

    const getTele = await telephoneController.getTelephoneNumber(req, res, next);

    chai.expect(next).to.not.have.been.called;
    chai.expect(getTele.message).to.equal(expectedError);
  });

  it('success on company name check', async () => {
    await telephoneController.checkCompanyName(req, res, next);
    chai.expect(next).to.have.been.called;
  });

  it('fail on company name check', async () => {
    req.query.company = undefined;
    const expectedError = 'Bad request: Missing company name.';

    const checkCompanyName = await telephoneController.checkCompanyName(req, res, next);

    const nextCallArgument = next.getCall(0).args[0];
    chai.expect(nextCallArgument.message).to.equal(expectedError);
  });
});
