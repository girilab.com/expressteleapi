const express = require('express');
const telephoneController = require('../controllers/telephone.controller');

const router = express.Router();

router
  .route('/')
  .get(
    telephoneController.checkCompanyName,
    telephoneController.getTelephoneNumber
  );

module.exports = router;
