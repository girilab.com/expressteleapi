# teleAPI

Express.js API pour rechercher le numéro de téléphone des entreprises françaises.


## Available Scripts

Vous pouvez exécuter

### `npm start`

Exécute l'application sur http://localhost:5001


### API

Pour obtenir le numéro de téléphone d'une entreprise correspondant au paramètre `company`

GET Request
http://localhost:5001/api/v1/telephone?company=experdeco

Response:
{
  "status": "success",
  "telephone": "01 44 61 87 87"
}

### `npm test`
Exécutez le test en utilisant Mocha, chai, Sinon.

```
> october@1.0.0 test /Applications/MAMP/htdocs/test/teleAPI
> mocha

App running on port 5001...


  Test API Route GET /api/v1/telephone
    ✓ it should GET the response with telephone number for route /api/v1/telephone (705ms)
    ✓ it should fail on missing url parameter "company"
    ✓ it should fail for unknown routes

  Test telephone controller
    ✓ called res.json with success
    ✓ fail on no data from google map platform API
    ✓ fail on unexpected error
    ✓ success on company name check
    ✓ fail on company name check


  8 passing (824ms)
```


## Ci-joint un fichier API compatible avec l'outil de développement d'API POSTMAN.
Nom de fichier: october.postman_collection.json
