/**
  * Send Error in Development Environment
  */
const sendErrorDev = (err, res) => {
  res.status(err.statusCode).json({
    status: err.status,
    message: err.message,
    error: err,
    stack: err.stack
  });
}

/**
  * Send Error in Production Environment
  */
const sendErrorProd = (err, res) => {
  if (err.isOperational) {
    // Trusted Operational error
    res.status(err.statusCode).json({
      status: err.status,
      message: err.message
    });
  } else {
    // Programming or other unknown error. Do not leak details to the client.
    // Use a logging library instead of console.log
    console.log('Logging: ERROR', err);
    res.status(500).json({
      status: 'error',
      message: 'Something went wrong!'
    });
  }
}

/**
  * Global Error Handler
  */
module.exports = (err, req, res, next) => {
  err.statusCode = err.statusCode || 500;
  err.status = err.status || 'error';

  if (process.env.NODE_ENV === 'development') {
    sendErrorDev(err, res);
  } else if (process.env.NODE_ENV === 'production') {
    sendErrorProd(err, res);
  }
}
