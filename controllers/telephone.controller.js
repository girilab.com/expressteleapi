const dotenv = require('dotenv');
const axios = require('axios');

const AppError = require('../utils/appError');
const { searchCompanyByName, searchDetailsByCompanyId } = require('../services/places.service');

/**
  * Gets the company Id using the places service
  */
const getCompanyId = async (name, next) => {
  const data = await searchCompanyByName(name);
  if (!data) {
    console.log('Logging: No company');
    return next(new AppError('No company found using this API', 404));
  }

  return data.results[0].place_id;
}

/**
  * Gets the telephone number using the places service
  */
const getCompanyTelephone = async (id, next) => {

  const details = await searchDetailsByCompanyId(id);

  if (!details) {
      console.log('Logging: No telephone');
    return next(new AppError('No telephone found using this API', 404));
  }

  return details.data.result.formatted_phone_number;
}

/**
  * Checks if the company name is present in the URL params
  */
exports.checkCompanyName = (req, res, next) => {
  if (!req.query.company) {
     return next(new AppError('Bad request: Missing company name.', 404));
  }
  next();
}

/**
  * Gets the telephone number of a company
  */
exports.getTelephoneNumber = async (req, res, next) => {
 try {
   const companyId = await getCompanyId(req.query.company, next);
   const telephone = await getCompanyTelephone(companyId, next);

   res.status(200).json({
     status: 'success',
     telephone,
   });
 }
 catch (err) {
   console.log('Logging: Catch ERROR', err);
   return new AppError('Oops. Something went wrong!', 404);
 }
}
