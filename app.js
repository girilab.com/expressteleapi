const express = require('express');

const AppError = require('./utils/appError');
const globalErrorHandler = require('./controllers/error.controller');
const telephoneRouter = require('./routes/telephone.route');

const app = express();

// MIDDLEWARES
app.use(express.json());

// ROUTES
app.use('/api/v1/telephone', telephoneRouter);

// ERROR HANDLER
app.all('*', (req, res, next) => {
  next(new AppError(`Cannot find ${req.originalUrl} on this server!`, 404));
});
app.use(globalErrorHandler);

module.exports = app;
